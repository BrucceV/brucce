<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Ejercicio Propuesto</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
        <!-- Styles -->
        <style>
        .fakeimg {
          height: 200px;
          background: #aaa;
        }
        p{
          text-align: center;
        }
        .section {
          width: 100%;
          display: flex;
          align-items: center;
        }
        .menu {
          text-align: center;
          align-items: center;
          background-image:url(img/fondo-2.jpg);
          width: 100%;
          height: 10%;
          font-family: "Times New Roman",Times, serif;
          font-style: italic;
        }
        .parte1 {
          width: 100%;
          float: justify;
        }
        #menu2 {
          padding:0;
          margin:0;
          list-style-type:none;
          height:100%;
          width:100%;
        }
        #menu2 li a {
          width:674.5px;
          height:379px;
          float:left;
          border-right:2px solid #fff;
          overflow: hidden;
        }
        div.parte1 img { max-width: 100%; }
        .container{
          text-align: center;
        }
        div{
        	margin: auto;
        }
        #boletin{
          padding:15px;
          color:#ffffff;
          background:black;
          border-bottom:#1293d4 5px solid;
        }

        #boletin h1{
          float:center;
        }
        #boletin .resaltado{
          color:#1293d4;
          font-weight:bold;
        }
        #boletin .cuenta {
          margin-left: 50px;
          float:right;
          margin-top:15px;
        }

        #boletin a {

             padding:4px;
             height:25px;
             width:250px;
        }

        </style>
        <!-- Styles -->
    </head>
    <body>
      <section id="boletin">
            <div class="contenedor">
                <div id="marca">
                  <h1><span class="resaltado">Los videojuegos mas esperados del 2019</span></h1>
                </div>
            </form>
          </div>
        </section>
      <div class="parte1">
        <ul id="menu2">
          <li><a><img src="img/fondo1.jpg"></a></li>
          <li><a><img src="img/fondo2.jpg"></a></li>
        </ul>
      </div>

      <nav class="navbar navbar-expand-sm bg-dark navbar-dark">
        <a class="navbar-brand" href="#"><img src="img/fondo0.jpg" weight="50" height="50">Los mejores videojuegos ya llegaron a PC</a>
      </nav>

      <div class="container" >
        <div class="jumbotron text-center">
          <div class="col-sm-8">
            <h2>Este 2019 esta lleno de sorpresas</h2>
            <h5>El 2019 parace ser un año prometedor desde videojuegos remasterizados hasta lanzamientos llegando a PC, PS4, Xbox One y Swicht. Por ahora, estos son los que más hemos disfrutado y los que esperamos con más ganas, informate acera de los proximos estrenos mas esperados.</h5>
            <div class="fakeimg"><img src="img/fondo-1.jpg"  width=100% height=100%></div>
            <br>
            <p>De entre todos los que ya están disponibles, repasamos a continuación aquellos juegos de PC que no deberías perderte.</p>
            <br>
            <h2>Ape Out</h2>
            <h5>28 de febrero de 2019</h5>
            <div class=""> <img src="img/fondo3.jpg"  width=90% height=100%></div>
            <br>
            <p>Pese a que en nuestro análisis de Ape Out destacamos que el juego pecaba de falta de opciones para alargar su vida útil, el festival que ofrece a nivel jugable, visual y sonoro bien merece la oportunidad de un único pero muy recomendable paseo. Un título de acción tan frenético como divertido.</p>
            <br>
            <h2>RESIDENT EVIL 2 REMAKE</h2>
            <h5>25 de enero de 2019</h5>
            <div class=""> <img src="img/fondo4.jpg"  width=90% height=100%></div>
            <br>
            <p>Otra de las grandes muestras de hasta dónde ha conseguido llegar la buena mano de Capcom. Resident Evil 2 podría haber sido muchas cosas, pero se nos hacía difícil imaginar un escenario tan positivo. El remake acaba encorsetado por unas inevitables costuras, pero lejos de suponer un problema, el homenaje al juego de PSX les pone un lazo para que las recibamos como regalo.</p>
            <h2>Metro Exodus</h2>
            <h5>15 de febrero de 2019</h5>
            <div class=""> <img src="img/fondo5.jpg"  width=90% height=100%></div>
            <br>
            <p>Se acerca el día en el que Artyom descubrirá si hay vida fuera del metro y 4A Games lo tiene todo dispuesto para el lanzamiento de Metro Exodus. Aunque claro, si vamos a abordarlo en PC queda por resolver un pequeño pero importante detalle por nuestra parte : ¿tenemos el equipo adecuado?.Deep Silver y 4A Games acaban de anunciar los requisitos del próximo episodio de la saga Metro haciendo distinción entre nada menos que cuatro calidades: mínima (1080p a 30 fps), recomendada (1080p a 60 fps), alta (1440p a 60 fps) y Extrema (4k a 60 fps).</p>
          </div>
        </div>
      </div>
        <p>Copyright Brucce 2019-2019 - Prohibida la reproducción total o parcial de estos contenidos sin el permiso expreso de los autores.</p>
    </body>
</html>
